import React, { Component } from 'react';
import { AppState } from 'react-native';
import { Provider } from 'react-redux';
import { setBadgeNumber } from './src/services/push-notifications';
import { Font, AppLoading } from 'expo';

import Navigation from './src/components/Navigation';
import store from './src/config/store';
import timeoutfix from './src/time-out-fix-util';
console.disableYellowBox = true;

class App extends Component {
  state = {
    loading: true,
  };

  async componentWillMount() {
    timeoutfix();
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
    });
    this.setState({ loading: false });
  }

  async componentDidMount() {
    setBadgeNumber(0);
    AppState.addEventListener('change', this.handleAppStateChange);
    this.setState({
      loading: true,
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'active') {
      setBadgeNumber(0);
    }
  };

  render() {
    if (this.state.loading) {
      return <AppLoading />;
    }
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}

export default App;
