# Introduction

The goal of the Notifyd Device is to allow end users(clinicians) to receive, and respond to mobile notifications sent from an Electronic Medical Records System or Systems.

- **Aware:** TODO.
- **Intrigued:** TODO.
- **Trusting:** TODO.
- **Convinced:** TODO.

# Technology

This app is written in Javascript and makes use of a number of Javascript libraries including Expo for rapid platform development, Redux for application state management and firebase for authentication and persistence services.

# Folder Structure

- **app.json:** Contains configuration details for the expo development platform.
- **eslint.json:** Contains code linting rules when running eslint automatic linter.
- **package.json:** Contains list of project dependencies and various configuration details.
- **/assets:** Contains all static assets including logos, splash screens, icons and logos.

- **/src:** Contains all Javascript files assets including components, screens, actions, reducers, and services.

- **/components/screens:** Contains one complete view composed of several small components.
- **/components/common:** Contains building blocks of screens, like headers, buttons, inputs etc.
- **/components/forms:** Contains reusable forms made of several components. Make up the bulk of auth screens
- **/services:** Contains independent objects and methods that make use of external services like firebase, expo.
- **/reducers:** Contains functions that return a piece of application state.
- **/actions:** Contains objects that update application state..
