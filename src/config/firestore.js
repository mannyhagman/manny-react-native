import firebase from './firebase';
import 'firebase/firestore';

firebase.firestore().settings({
	timestampsInSnapshots: true,
});

export default firebase.firestore();
