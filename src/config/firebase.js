import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyCB3c8R2Zu7yeFhRBzmPvhFOG8ocW88iN0',
  authDomain: 'notifydapp-6b6a8.firebaseapp.com',
  databaseURL: 'https://notifydapp-6b6a8.firebaseio.com',
  projectId: 'notifydapp-6b6a8',
};

export default firebase.initializeApp(firebaseConfig);
