import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 5,
    marginTop: 30,
    backgroundColor: '#1C4367',
  },
  imageViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    width: '95%',
    marginTop: 160,
    marginBottom: 20,
    resizeMode: 'contain',
  },
  iconFontStyle: {
    fontSize: 38,
  },
  viewPaddingStyle: {
    padding: 20,
  },
  marginBottomStyle: {
    marginBottom: 40,
  },
  viewWrapperStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20,
  },
  smallPaddinStyle: {
    padding: 3,
  },
  red: {
    paddingTop: 30,
    color: 'red',
  },
  blue300: {
    color: '#1C4367',
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
