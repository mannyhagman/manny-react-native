import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Form, H1, Icon, Input, Item, Text, Spinner } from 'native-base';
import { Image, View } from 'react-native';
import { signUpUser } from '../../actions/actions-signup';
import { verifyAuthState } from '../../actions/actions-login';
import { emailChanged, passwordChanged } from '../../actions/actions-inputs';
import { clearMessage } from '../../actions/actions-login';
import { blue300 } from '../common/colors';
import styles from './style';

class SignupScreen extends Component {
  componentDidMount() {
    this.props.clearMessage();
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.user) !== JSON.stringify(nextProps.user)) {
      this.props.navigation.navigate('Confirm');
    }
  }

  handlePasswordChanged = (text) => {
    this.props.passwordChanged(text);
  };

  handleEmailChanged = (text) => {
    this.props.emailChanged(text);
  };

  handleSignUpUser = () => {
    const { email, password } = this.props;
    this.props.signUpUser(email, password);
  };

  renderButton = () => {
    const { loading } = this.props;
    const { buttonStyle } = styles;

    if (loading) {
      return <Spinner color={blue300} />;
    }

    return (
      <Button onPress={this.handleSignUpUser} style={buttonStyle} full large>
        <Text>Signup</Text>
      </Button>
    );
  };

  render() {
    const {
      imageStyle,
      imageViewStyle,
      iconFontStyle,
      marginBottomStyle,
      viewWrapperStyle,
      viewPaddingStyle,
      smallPaddingStyle,
      red,
    } = styles;
    return (
      <Container>
        <View style={imageViewStyle}>
          <Image style={imageStyle} source={require('../../../assets/logo.png')} />
          <View style={marginBottomStyle}>
            <H1 textAlign="center" padding>
              Welcome to Notifyd
            </H1>
          </View>
        </View>

        <Form>
          <Item>
            <Icon style={iconFontStyle} active name="md-mail" />
            <Input
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={this.handleEmailChanged}
              placeholder="email@example.com"
              value={this.props.email}
            />
          </Item>

          <Item style={smallPaddingStyle}>
            <Icon style={iconFontStyle} active name="md-lock" />
            <Input
              secureTextEntry
              autoCapitalize="none"
              autoCorrect={false}
              placeholder="Please enter your temporary password"
              onChangeText={this.handlePasswordChanged}
              value={this.props.password}
            />
          </Item>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={red}>{this.props.error}</Text>
          </View>
          <View style={viewPaddingStyle}>
            {this.renderButton()}
            <View style={viewWrapperStyle}>
              <Button onPress={() => this.props.navigation.navigate('Login')} transparent>
                <Text>Need to login instead?</Text>
              </Button>
            </View>
          </View>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  email: state.auth.email,
  password: state.auth.password,
  user: state.auth.user,
  error: state.auth.error,
  loading: state.auth.loading,
});

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  signUpUser,
  clearMessage,
  verifyAuthState,
})(SignupScreen);
