import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Form, H1, Icon, Input, Item, Text, Spinner } from 'native-base';
import { Image, View } from 'react-native';
import styles from './style';
import { blue300 } from '../common/colors';
import { emailChanged, passwordChanged, confirmPasswordChanged } from '../../actions/actions-inputs';
import { clearMessage, loginUser } from '../../actions/actions-login';
import { updatePassword } from '../../actions/actions-update-password';
import { Alert } from 'react-native';

class ConfirmPasswordScreen extends Component {
  componentDidMount() {
    this.props.clearMessage();
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.user) !== JSON.stringify(nextProps.user)) {
      Alert.alert('Password Update Successful');
      this.props.navigation.navigate('Login');
    }
  }

  handleEmailChange = (text) => {
    this.props.emailChanged(text);
  };

  handlePasswordChange = (text) => {
    this.props.passwordChanged(text);
  };

  handleConfirmPasswordChanged = (text) => {
    this.props.confirmPasswordChanged(text);
  };

  handleUpdatePassword = () => {
    if (this.props.password !== this.props.confirmPassword) {
      return console.log('passwords must match');
    }
    this.props.updatePassword(this.props.confirmPassword);
  };

  renderButton = () => {
    const { buttonStyle } = styles;
    const { loading } = this.props;

    if (loading) {
      return <Spinner color={blue300} />;
    }

    return (
      <Button onPress={this.handleUpdatePassword} style={buttonStyle} full large>
        <Text>Confirm Password</Text>
      </Button>
    );
  };

  render() {
    const {
      imageStyle,
      imageViewStyle,
      iconFontStyle,
      marginBottomStyle,
      viewWrapperStyle,
      viewPaddingStyle,
      smallPaddingStyle,
      red,
    } = styles;
    return (
      <Container>
        <View style={imageViewStyle}>
          <Image style={imageStyle} source={require('../../../assets/logo.png')} />
          <View style={marginBottomStyle}>
            <H1 textAlign="center" padding>
              Confirm Password
            </H1>
          </View>
        </View>

        <Form>
          <Item>
            <Icon style={iconFontStyle} active name="md-lock" />
            <Input
              secureTextEntry
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={this.handlePasswordChange}
              placeholder="Enter your new password"
              value={this.props.password}
            />
          </Item>

          <Item style={smallPaddingStyle}>
            <Icon style={iconFontStyle} active name="md-lock" />
            <Input
              secureTextEntry
              autoCapitalize="none"
              autoCorrect={false}
              placeholder="Please confirm your new password"
              onChangeText={this.handleConfirmPasswordChanged}
              value={this.props.confirmPassword}
            />
          </Item>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={red}>{this.props.error}</Text>
          </View>
          <View style={viewPaddingStyle}>
            {this.renderButton()}
            <View style={viewWrapperStyle}>
              <Button onPress={() => this.props.navigation.navigate('Login')} transparent>
                <Text>Need to login instead?</Text>
              </Button>
            </View>
          </View>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  email: state.auth.email,
  password: state.auth.password,
  user: state.auth.user,
  error: state.auth.error,
  loading: state.auth.loading,
  confirmPassword: state.auth.confirmPassword,
});

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser,
  clearMessage,
  confirmPasswordChanged,
  updatePassword,
})(ConfirmPasswordScreen);
