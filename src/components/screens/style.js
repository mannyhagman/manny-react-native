import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	buttonStyle: {
		borderRadius: 5,
		marginTop: 5,
		backgroundColor: '#1C4367',
	},
	imageViewStyle: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	imageStyle: {
		width: '95%',
		marginTop: 80,
		marginBottom: 20,
		resizeMode: 'contain',
	},
	iconFontStyle: {
		fontSize: 38,
	},
	viewPaddingStyle: {
		padding: 20,
	},
	marginBottomStyle: {
		marginBottom: 20,
	},
	viewWrapperStyle: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginTop: 20,
	},
	smallPaddinStyle: {
		padding: 3,
	},
	red: {
		paddingTop: 30,
		color: 'red',
	},
	blue300: {
		color: '#1C4367',
	},
});
