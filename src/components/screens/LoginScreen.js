import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Form, H1, Icon, Input, Item, Text, Spinner } from 'native-base';
import { Image, View } from 'react-native';
import { blue300 } from '../common/colors';
import { emailChanged, passwordChanged } from '../../actions/actions-inputs';
import { clearMessage, loginUser, verifyAuthState } from '../../actions/actions-login';

import styles from './style';

class LoginScreen extends Component {
  componentDidMount() {
    this.props.clearMessage();
    this.props.verifyAuthState();
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.user) !== JSON.stringify(nextProps.user)) {
      this.props.navigation.navigate('Notifications');
    }
  }

  handlePasswordChange = (text) => {
    this.props.passwordChanged(text);
  };

  handleEmailChange = (text) => {
    this.props.emailChanged(text);
  };

  handleLoginPress = () => {
    const { email, password } = this.props;
    this.props.loginUser(email, password);
  };

  handlePasswordResetPress = () => {
    this.props.navigation.navigate('Reset');
  };

  renderButton = () => {
    const { buttonStyle } = styles;

    if (this.props.loading) {
      return <Spinner color={blue300} />;
    }

    return (
      <Button onPress={this.handleLoginPress} style={buttonStyle} full large>
        <Text>Login</Text>
      </Button>
    );
  };

  render() {
    const {
      imageStyle,
      imageViewStyle,
      iconFontStyle,
      marginBottomStyle,
      viewWrapperStyle,
      viewPaddingStyle,
      smallPaddingStyle,
      red,
    } = styles;

    return (
      <Container>
        <View style={imageViewStyle}>
          <Image style={imageStyle} source={require('../../../assets/logo.png')} />
          <View style={marginBottomStyle}>
            <H1 textAlign="center" padding>
              Welcome to Notifyd
            </H1>
          </View>
        </View>
        <Form>
          <Item>
            <Icon style={iconFontStyle} active name="md-mail" />
            <Input
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={this.handleEmailChange}
              placeholder="email@example.com"
              value={this.props.email}
            />
          </Item>

          <Item style={smallPaddingStyle}>
            <Icon style={iconFontStyle} active name="md-lock" />
            <Input
              secureTextEntry
              autoCapitalize="none"
              autoCorrect={false}
              placeholder="Please enter your password"
              onChangeText={this.handlePasswordChange}
              value={this.props.password}
            />
          </Item>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={red}>{this.props.error}</Text>
          </View>
          <View style={viewPaddingStyle}>
            {this.renderButton()}
            <View style={viewWrapperStyle}>
              <Button onPress={() => this.props.navigation.navigate('Signup')} transparent>
                <Text>Need to signup instead?</Text>
              </Button>
              <Button onPress={this.handlePasswordResetPress} transparent>
                <Text>Reset Password?</Text>
              </Button>
            </View>
          </View>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  email: state.auth.email,
  password: state.auth.password,
  user: state.auth.user,
  error: state.auth.error,
  loading: state.auth.loading,
  fetching: state.notifications.loading,
});

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser,
  clearMessage,
  verifyAuthState,
})(LoginScreen);
