import React, { Component } from 'react';
import { Button, Container, Form, H1, Icon, Input, Item, Text, Spinner } from 'native-base';
import { Image, View } from 'react-native';
import { connect } from 'react-redux';
import { clearMessage } from '../../actions/actions-login';
import { emailChanged } from '../../actions/actions-inputs';
import { resetPassword } from '../../actions/actions-reset-password';
import styles from './style';

const blue300 = '#1C4367';

class ResetPasswordScreen extends Component {
  componentDidMount() {
    this.props.clearMessage();
  }
  handleEmailChanged = (text) => {
    this.props.emailChanged(text);
  };

  handlePasswordReset = () => {
    const { email } = this.props;
    this.props.resetPassword(email);
  };

  renderButton = () => {
    const { loading } = this.props;
    const { buttonStyle } = styles;

    if (loading) {
      return <Spinner color={blue300} />;
    }
    return (
      <Button onPress={this.handlePasswordReset} style={buttonStyle} full large>
        <Text>Reset Password</Text>
      </Button>
    );
  };

  renderMessage = () => {
    const { error, message } = this.props;
    console.log(message);
    if (error) {
      return <Text style={{ color: 'red' }}>{error}</Text>;
    }
    return <Text style={{ color: 'blue' }}>{message}</Text>;
  };

  render() {
    const { email } = this.props;
    const { viewPaddingStyle, viewWrapperStyle } = styles;
    return (
      <Container>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{
              width: '95%',
              marginTop: 80,
              marginBottom: 20,
              resizeMode: 'contain',
            }}
            source={require('../../../assets/logo.png')}
          />
          <View style={{ marginBottom: 40 }}>
            <H1 textAlign="center" padding>
              Request Password Email
            </H1>
          </View>
        </View>
        <Form>
          <Item style={{ padding: 3 }}>
            <Icon style={{ fontSize: 38 }} active name="md-mail" />
            <Input
              placeholder="email@example.com"
              onChangeText={this.handleEmailChanged}
              autoCapitalize="none"
              autoCorrect={false}
              value={email}
            />
          </Item>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>{this.renderMessage()}</View>
          <View style={viewPaddingStyle}>
            {this.renderButton()}
            <View style={viewWrapperStyle}>
              <Button onPress={() => this.props.navigation.navigate('Login')} transparent>
                <Text>Back to Login</Text>
              </Button>
            </View>
          </View>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    email: state.auth.email,
    loading: state.auth.loading,
    screen: state.auth.screen,
    error: state.auth.error,
    message: state.auth.message,
  };
};

export default connect(mapStateToProps, {
  resetPassword,
  emailChanged,
  clearMessage,
})(ResetPasswordScreen);
