import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar } from 'react-native';
import { Container } from 'native-base';
import { fetchNotifications } from '../../actions/actions-notifications';
import Header from '../common/Header/Header';
import HeaderTabs from '../common/HeaderTabs/HeaderTabs';
import Avatar from '../common/Avatar/Avatar';
import registerForPushNotificationsAsync from '../../services/push-notifications';
import { PUSH_ENDPOINT } from '../../constants';
import axios from 'axios';

class NotificationsScreen extends Component {
  componentDidMount() {
    const { user, fetchNotifications } = this.props;
    const { uid } = user;
    fetchNotifications(uid);
    registerForPushNotificationsAsync()
      .then((expoDeviceToken) =>
        axios.post(PUSH_ENDPOINT, {
          uid,
          expoDeviceToken,
        }),
      )
      .catch((error) => console.log(error));
  }

  renderNotifications = () => {
    return (
      <Container>
        <StatusBar hidden />
        <Header hasTabs headerText="Notifications" />
        <Avatar />
        <HeaderTabs />
      </Container>
    );
  };
  render() {
    return this.renderNotifications();
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default connect(mapStateToProps, {
  fetchNotifications,
})(NotificationsScreen);
