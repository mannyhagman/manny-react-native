import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Text, H3 } from 'native-base';
import List from './List';
import ListItem from '../ListItem/ListItem';
import styles from './style';

class SavedNotificationsList extends Component {
  renderZeroNotifications = () => {
    const { centered, textColorBlue } = styles;
    return (
      <Content contentContainerStyle={centered}>
        <Text style={textColorBlue}>
          <H3>You have no saved notifications</H3>
        </Text>
      </Content>
    );
  };

  renderNotifications = () => {
    const { listContainerStyle } = styles;
    const { savedNotifications } = this.props;
    return (
      <Content padder style={listContainerStyle}>
        <List
          data={savedNotifications}
          keyExtractor={(item) => item.id}
          renderItem={({ item: { id, data, timestamp, response } }) => (
            <ListItem
              id={id}
              key={id}
              title={data.title}
              message={data.message}
              timestamp={timestamp}
              response={response}
            />
          )}
        />
      </Content>
    );
  };

  render() {
    if (this.props.savedNotifications.length === 0) {
      return this.renderZeroNotifications();
    }
    return this.renderNotifications();
  }
}

const mapStateToProps = (state) => ({
  savedNotifications: state.notifications.notifications.filter((item) => item.response),
  uid: state.auth.user.uid,
  loading: state.notifications.loading,
});

export default connect(mapStateToProps)(SavedNotificationsList);
