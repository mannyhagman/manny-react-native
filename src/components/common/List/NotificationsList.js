import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Text, H3 } from 'native-base';
import List from './List';
import ListItem from '../ListItem/ListItem';
import styles from './style';

class NotificationsList extends Component {
  renderZeroNotifications = () => {
    const { centered, textColorBlue } = styles;
    return (
      <Content contentContainerStyle={centered}>
        <Text style={textColorBlue}>
          <H3>You have no new notifications</H3>
        </Text>
      </Content>
    );
  };

  renderNotifications = () => {
    const { listContainerStyle } = styles;
    const { notifications } = this.props;
    return (
      <Content padder style={listContainerStyle}>
        <List
          data={notifications}
          keyExtractor={(item) => item.id}
          renderItem={({ item: { id, actions, data, timestamp, responseURL, organization } }) => (
            <ListItem
              id={id}
              key={id}
              title={data.title}
              message={data.message}
              timestamp={timestamp}
              actions={actions}
              responseURL={responseURL}
              organization={organization}
            />
          )}
        />
      </Content>
    );
  };

  render() {
    if (this.props.notifications.length === 0) {
      return this.renderZeroNotifications();
    }
    return this.renderNotifications();
  }
}

const mapStateToProps = (state) => ({
  notifications: state.notifications.notifications.filter((item) => !item.response),
  loading: state.notifications.loading,
});

export default connect(mapStateToProps)(NotificationsList);
