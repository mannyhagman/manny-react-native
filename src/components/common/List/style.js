import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	listContainerStyle: {
		backgroundColor: '#E6E9EC',
	},
	centered: {
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 100,
	},
});

export default styles;
