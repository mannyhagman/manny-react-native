import { FlatList } from 'react-native';
import React, { Component } from 'react';

class List extends Component {
	render() {
		const { data, renderItem, keyExtractor } = this.props;
		return <FlatList data={data} keyExtractor={keyExtractor} renderItem={renderItem} />;
	}
}

export default List;
