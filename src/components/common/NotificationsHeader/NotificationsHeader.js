import React from 'react';
import { Content, Text, H3 } from 'native-base';
import { blue300 } from '../../../config/colors';

const NotificationsHeader = ({ zeroNotificationsText }) => {
  return (
    <Content contentContainerStyle={centered}>
      <Text style={{ color: blue300 }}>
        <H3>{zeroNotificationsText}</H3>
      </Text>
    </Content>
  );
};

export default NotificationsHeader;
