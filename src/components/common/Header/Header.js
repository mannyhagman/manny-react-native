import React, { Component } from 'react';
import { Body, Button, Header, Left, Right, Title } from 'native-base';
import { withNavigation } from 'react-navigation';
import { signOutUser } from '../../../actions/actions-signout';
import { connect } from 'react-redux';
import styles from './style';

class Heading extends Component {
  handleSignOutPress = () => {
    this.props.signOutUser();
    this.props.navigation.popToTop();
  };
  render() {
    const { headerBackgroundStyle, centerNav, white } = styles;
    return (
      <Header style={headerBackgroundStyle}>
        <Left style={centerNav}>
          <Button transparent />
        </Left>
        <Body style={centerNav}>
          <Title style={white}>{this.props.headerText}</Title>
        </Body>
        <Right style={centerNav}>
          <Button onPress={this.handleSignOutPress} transparent>
            <Title style={white}>Sign Out</Title>
          </Button>
        </Right>
      </Header>
    );
  }
}

export default connect(null, {
  signOutUser,
})(withNavigation(Heading));
