import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerBackgroundStyle: {
    backgroundColor: '#1C4367',
    borderBottomColor: '#1C4367',
  },
  white: {
    color: 'white',
  },
  centerNav: {
    flex: 1,
  },
});

export default styles;
