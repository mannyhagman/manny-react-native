import { StyleSheet } from 'react-native';
import { gray300, blue300 } from '../colors';

const styles = StyleSheet.create({
  cardItemStyle: {
    backgroundColor: gray300,
    borderColor: blue300,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
  },
  cardTextStyle: {
    color: '#50a5ed',
  },
  cardStyle: {
    borderColor: blue300,
    borderWidth: 2,
  },
  cardColStyle: {
    backgroundColor: blue300,
  },
  timeStyle: {
    fontSize: 14,
    color: '#007aff',
    paddingTop: 0,
  },
  messageStyle: {
    color: 'black',
    paddingBottom: 0,
    marginBottom: 0,
  },
  textColorBlue: {
    color: blue300,
  },
});

export default styles;
