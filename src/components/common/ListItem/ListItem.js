import React, { Component } from 'react';
import { Card, CardItem, Text } from 'native-base';
import { Col, Grid, Row } from 'react-native-easy-grid';
import moment from 'moment';
import { cardStyle, timeStyle, messageStyle } from './style';
import { blue300 } from '../colors';
import { Alert } from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import { usersCollection, notifications, RESPONSES_URL } from '../../../constants';
import styles from './style';

class ListItem extends Component {
  handleZeroPostResponse = () => {
    const notificationData = {
      uid: this.props.uid,
      responseURL: this.props.responseURL,
      notificationID: this.props.id,
      orgId: this.props.organization,
      response: 'Viewed',
    };
    this.handleUserResponse(notificationData);
  };

  handlePostResponse = () => {
    const notificationData = {
      uid: this.props.uid,
      responseURL: this.props.responseURL,
      notificationID: this.props.id,
      orgId: this.props.organization,
      response: this.props.actions[0].text,
    };

    this.handleUserResponse(notificationData);
  };

  handleSecondaryPostResponse = () => {
    const notificationData = {
      uid: this.props.uid,
      responseURL: this.props.responseURL,
      notificationID: this.props.id,
      orgId: this.props.organization,
      response: this.props.actions[1].text,
    };

    this.handleUserResponse(notificationData);
  };

  updateResponse = (response) => {
    const { uid, id } = this.props;
    usersCollection.doc(uid).collection(notifications).doc(id).update({
      response,
    });
  };

  handleUserResponse = (notificationData) => {
    this.updateResponse(notificationData.response);
    this.postResponse(notificationData);
  };

  postResponse = (notificationData) => {
    axios
      .post(RESPONSES_URL, notificationData)
      .then((result) => {
        alert('Your response has been received');
      })
      .catch((error) => {
        alert(error);
      });
  };

  handleZeroActionPress = () => {
    Alert.alert(
      `Confirm your response of 'Viewed`,
      'By sending this you confirm you have viewed and understand this message',
      [
        {
          text: 'Confirm',
          onPress: this.handleZeroPostResponse,
        },
        {
          text: 'Cancel',
          onPress: () => alert('Your response has been cancelled'),
          color: 'red',
          style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  };

  handleDefaultActionPress = () => {
    Alert.alert(
      `Confirm your response of '${this.props.actions[0].text.toLowerCase()}?'`,
      'By sending this you confirm you have viewed and understand this message',
      [
        {
          text: 'Confirm',
          onPress: this.handlePostResponse,
        },
        {
          text: 'Cancel',
          onPress: () => alert('Your response has been cancelled'),
          color: 'red',
          style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  };

  handleSecondaryActionPress = () => {
    Alert.alert(
      `Confirm your response of '${this.props.actions[1].text.toLowerCase()}'?`,
      'By sending this you confirm you have viewed and understand this message',
      [
        {
          text: 'Confirm',
          onPress: this.handleSecondaryPostResponse,
        },
        {
          text: 'Cancel',
          onPress: () => alert('Your response has been canceled'),
          color: 'red',
          style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  };

  renderActions = (actions) => {
    let result = null;
    if (actions.length < 1 || !actions) {
      result = (
        <Row>
          <Col style={styles.cardColStyle} size={100}>
            <CardItem
              style={{
                backgroundColor: '#1C4367',
                justifyContent: 'center',
              }}
              button
              onPress={this.handleZeroActionPress}
            >
              <Text style={{ color: 'white' }}>Viewed</Text>
            </CardItem>
          </Col>
        </Row>
      );
    } else if (actions.length === 1) {
      result = (
        <Row>
          <Col style={styles.cardColStyle} size={100}>
            <CardItem
              style={{
                backgroundColor: '#1C4367',
                justifyContent: 'center',
              }}
              button
              onPress={this.handleDefaultActionPress}
            >
              <Text style={{ color: 'white' }}>{actions[0].text}</Text>
            </CardItem>
          </Col>
        </Row>
      );
    } else {
      result = (
        <Row>
          <Col style={styles.cardColStyle} size={50}>
            <CardItem
              style={{
                backgroundColor: '#50a5ed',
                justifyContent: 'center',
              }}
              button
              onPress={this.handleDefaultActionPress}
            >
              <Text style={{ color: 'white' }}>{actions[0].text}</Text>
            </CardItem>
          </Col>
          <Col style={styles.cardColStyle} size={50}>
            <CardItem
              style={{
                backgroundColor: '#1C4367',
                justifyContent: 'center',
              }}
              button
              onPress={this.handleSecondaryActionPress}
            >
              <Text style={{ color: 'white' }}>{actions[1].text}</Text>
            </CardItem>
          </Col>
        </Row>
      );
    }
    return result;
  };

  renderTime = (timestamp) => {
    const numberTimestamp = Number(timestamp);
    const seconds = numberTimestamp * 1000;
    const time = parseInt(seconds);
    const now = moment().unix() * 1000;
    const date = moment.unix(seconds / 1000);
    const formattedTime = moment.unix(seconds / 1000).format('h:mma');
    const when = Math.abs(now - time) < 10000 ? 'just now' : moment.unix(seconds / 1000).fromNow();
    const params = {
      month: date.format('MMM'),
      day: date.format('D'),
      formattedTime,
      when,
    };
    return <Text style={timeStyle}>{params.when}</Text>;
  };

  renderListItem = () => {
    const { title, message, timestamp, response, actions } = this.props;

    if (!response) {
      return (
        <Card style={cardStyle}>
          <Grid>
            <Row>
              <CardItem header>
                <Text style={{ color: '#1C4367' }}>{title}</Text>
              </CardItem>
            </Row>
            <Row>
              <CardItem>
                <Text note style={messageStyle}>
                  {message}
                </Text>
              </CardItem>
            </Row>
            <Row>
              <CardItem>
                <Text note style={messageStyle}>
                  {this.renderTime(timestamp.seconds)}
                </Text>
              </CardItem>
            </Row>
            <Row>{this.renderActions(actions)}</Row>
          </Grid>
        </Card>
      );
    }
    return (
      <Card style={cardStyle}>
        <Grid>
          <Row>
            <CardItem header>
              <Text style={{ color: blue300 }}>{title}</Text>
            </CardItem>
          </Row>
          <Row>
            <CardItem>
              <Text note style={messageStyle}>
                {message}
              </Text>
            </CardItem>
          </Row>
          <Row>
            <CardItem>
              <Text style={timeStyle}>{this.renderTime(timestamp.seconds)}</Text>
            </CardItem>
          </Row>
          <Row>
            <CardItem>
              <Text note style={{ color: 'black' }}>
                Your response:
              </Text>
              <Text style={{ color: 'blue' }}> {this.props.response}</Text>
            </CardItem>
          </Row>
        </Grid>
      </Card>
    );
  };

  render() {
    return this.renderListItem();
  }
}

const mapStateToProps = (state) => {
  return {
    uid: state.auth.user.uid,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(ListItem);
