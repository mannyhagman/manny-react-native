import React, { Component } from 'react';
import { Tab, Tabs, Text, Content } from 'native-base';
import { connect } from 'react-redux';
import SavedNotificationsList from '../List/SavedNotificationsList';
import NotificationsList from '../List/NotificationsList';
import { blue300 } from '../colors';

const TabHeading = ({ children }) => {
  <Content>{children}</Content>;
};

class HeaderTabs extends Component {
  render() {
    const { unread, saved } = this.props;
    return (
      <Tabs>
        <Tab
          heading={
            <TabHeading style={{ backgroundColor: 'white' }}>
              <Text style={{ color: blue300 }}>New {unread}</Text>
            </TabHeading>
          }
        >
          <NotificationsList />
        </Tab>
        <Tab
          heading={
            <TabHeading style={{ backgroundColor: 'white' }}>
              <Text style={{ color: blue300 }}>Saved {saved}</Text>
            </TabHeading>
          }
        >
          <SavedNotificationsList />
        </Tab>
      </Tabs>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    unread: state.notifications.notifications.filter((item) => !item.response).length,
    saved: state.notifications.notifications.filter((item) => item.response).length,
  };
};

export default connect(mapStateToProps)(HeaderTabs);
