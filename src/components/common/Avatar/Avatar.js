import React from 'react';
import { Thumbnail } from 'native-base';
import { View } from 'react-native';
import styles from './style';

const Avatar = () => {
  const { containerStyle, thumbnailStyle } = styles;
  return (
    <View style={containerStyle}>
      <Thumbnail style={thumbnailStyle} source={require('./logo-full-color.png')} />
    </View>
  );
};

export default Avatar;
