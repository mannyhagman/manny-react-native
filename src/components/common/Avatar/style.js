import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#1C4367',
  },
  thumbnailStyle: {
    width: 220,
    backgroundColor: '#1C4367',
  },
});

export default styles;
