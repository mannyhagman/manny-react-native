import { StackNavigator, SwitchNavigator } from 'react-navigation';
import SignupScreen from './screens/SignupScreen';
import LoginScreen from './screens/LoginScreen';
import NotificationsScreen from './screens/NotificationsScreen';
import ResetPasswordScreen from './screens/ResetPasswordScreen';
import ConfirmPasswordScreen from './screens/ConfirmPasswordScreen';

const AuthNavigator = SwitchNavigator(
  {
    Login: { screen: LoginScreen },
    Signup: { screen: SignupScreen },
    Reset: { screen: ResetPasswordScreen },
    Confirm: { screen: ConfirmPasswordScreen },
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
      gesturesEnabled: false,
    },
    lazyLoad: true,
  },
);

const Navigation = StackNavigator(
  {
    Auth: { screen: AuthNavigator },
    Notifications: { screen: NotificationsScreen },
  },
  {
    initialRouteName: 'Auth',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
      gesturesEnabled: false,
    },
    lazyLoad: true,
  },
);

export default Navigation;
