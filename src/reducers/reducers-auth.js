import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  CLEAR_MESSAGE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAIL,
  VERIFY_AUTH_STATE,
  VERIFY_AUTH_STATE_SUCCESS,
  VERIFY_AUTH_STATE_FAIL,
  SIGNOUT_USER,
  CONFIRM_PASSWORD_CHANGED,
  UPDATE_PASSWORD,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_PASSWORD_FAIL,
  SIGNUP_USER,
  SIGNUP_USER_FAIL,
  SIGNUP_USER_SUCCESS,
} from '../constants/';

const INITIAL_STATE = {
  email: '',
  password: '',
  confirmPassword: '',
  error: '',
  loading: false,
  user: null,
  message: '',
  authenticated: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, error: '', email: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, error: '', password: action.payload };
    case CONFIRM_PASSWORD_CHANGED:
      return { ...state, error: '', confirmPassword: action.payload };
    case LOGIN_USER:
      return { ...state, error: '', loading: true };
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, authenticated: true, loading: false, user: action.payload };
    case LOGIN_USER_FAIL:
      return {
        ...state,
        error: 'Invalid email or password',
        password: '',
        loading: false,
        authenticated: false,
      };
    case SIGNUP_USER:
      return { ...state, error: '', loading: true };
    case SIGNUP_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, error: '', loading: false, user: action.payload };
    case SIGNUP_USER_FAIL:
      return { ...state, error: action.payload.message, loading: false };
    case RESET_PASSWORD:
      return {
        ...state,
        error: '',
        message: '',
        loading: true,
      };
    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        email: '',
        message: 'Reset request received. Please check your email for instructions',
        loading: false,
      };
    case RESET_PASSWORD_FAIL:
      return {
        ...state,
        email: '',
        error: 'Unable to reset password. Please try again',
        loading: false,
      };
    case UPDATE_PASSWORD:
      return {
        ...state,
        error: '',
        loading: true,
      };
    case UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        error: '',
        loading: false,
        result: action.payload,
      };
    case UPDATE_PASSWORD_FAIL:
      return {
        ...state,
        error: 'Error updating password',
        loading: false,
      };

    case VERIFY_AUTH_STATE:
      return {
        ...state,
        error: '',
        loading: true,
        authenticated: false,
      };
    case VERIFY_AUTH_STATE_SUCCESS:
      return {
        ...state,
        error: '',
        loading: false,
        authenticated: true,
        user: action.payload,
      };
    case VERIFY_AUTH_STATE_FAIL:
      return {
        ...state,
        loading: false,
        authenticated: false,
      };
    case SIGNOUT_USER:
      return {
        ...INITIAL_STATE,
        ...state,
        user: null,
        error: '',
        loading: false,
      };

    case CLEAR_MESSAGE:
      return {
        ...state,
        message: '',
        error: '',
      };

    default:
      return state;
  }
};
