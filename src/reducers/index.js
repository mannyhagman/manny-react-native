import { combineReducers } from 'redux';
import notifications from './reducers-notifications';
import auth from './reducers-auth';

export default combineReducers({
  notifications,
  auth,
});
