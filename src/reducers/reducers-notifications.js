import {
  FETCH_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_SUCCESS,
  FETCH_NOTIFICATIONS_FAIL,
} from '../constants';

const INITIAL_STATE = {
  notifications: [],
  loading: false,
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_NOTIFICATIONS:
      return { ...state, error: '', loading: true };
    case FETCH_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        notifications: action.payload,
      };
    case FETCH_NOTIFICATIONS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Failed to fetch notifications',
      };
    default:
      return {
        ...state,
        ...INITIAL_STATE,
      };
  }
};
