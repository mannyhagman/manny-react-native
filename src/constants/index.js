import firestore from '../config/firestore';

export const CLEAR_MESSAGE = 'CLEAR_MESSAGE';

export const EMAIL_CHANGED = 'EMAIL_CHANGED';
export const PASSWORD_CHANGED = 'PASSWORD_CHANGED';
export const CONFIRM_PASSWORD_CHANGED = 'CONFIRM_PASSWORD_CHANGED';

export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAIL = 'LOGIN_USER_FAIL';

export const SIGNUP_USER = 'SIGNUP_USER';
export const SIGNUP_USER_SUCCESS = 'SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_FAIL = 'SIGNUP_USER_FAIL';

export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_FAIL = 'RESET_PASSWORD_FAIL';

export const FETCH_NOTIFICATIONS = 'FETCH_NOTIFICATIONS';
export const FETCH_NOTIFICATIONS_SUCCESS = 'FETCH_NOTIFICATIONS_SUCCESS';
export const FETCH_NOTIFICATIONS_FAIL = 'FETCH_NOTIFICATIONS_FAIL';

export const UPDATE_PASSWORD = 'UPDATE_PASSWORD';
export const UPDATE_PASSWORD_SUCCESS = 'UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_FAIL = 'UPDATE_PASSWORD_FAIL';

export const SIGN_OUT_USER = 'SIGN_OUT_USER';

export const VERIFY_AUTH_STATE = 'VERIFY_AUTH_STATE';
export const VERIFY_AUTH_STATE_SUCCESS = 'VERIFY_AUTH_STATE_SUCCESS';
export const VERIFY_AUTH_STATE_FAIL = 'VERIFY_AUTH_STATE_FAIL';

export const orgs = 'organizations';
export const users = 'users';
export const notifications = 'notifications';
export const admins = 'admins';

export const usersCollection = firestore.collection(users);
export const orgsCollection = firestore.collection(orgs);
export const notificationsCollection = firestore.collection(notifications);
export const adminsCollection = firestore.collection(admins);

export const response = 'Viewed';

export const BASE_URL = 'https://us-central1-notifydapp-6b6a8.cloudfunctions.net/api/';
export const PUSH_ENDPOINT = 'https://notifyd-api-development.herokuapp.com/api/devices';
export const RESPONSES_URL = 'https://us-central1-notifydapp-6b6a8.cloudfunctions.net/api/responses';
