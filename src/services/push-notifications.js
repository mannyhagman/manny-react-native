import { Permissions, Notifications } from 'expo';

async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  let finalStatus = existingStatus;

  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  if (finalStatus !== 'granted') {
    return;
  }

  const expoDeviceToken = await Notifications.getExpoPushTokenAsync();

  return expoDeviceToken;
}

export default registerForPushNotificationsAsync;

export const setBadgeNumber = (number = 0) => {
  Notifications.setBadgeNumberAsync(number);
};
