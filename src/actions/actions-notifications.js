import {
  FETCH_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_SUCCESS,
  FETCH_NOTIFICATIONS_FAIL,
  usersCollection,
} from '../constants';

const fetchNotificationsFail = (dispatch, error) => {
  dispatch({
    type: FETCH_NOTIFICATIONS_FAIL,
    payload: error,
  });
};

const fetchNotificationsSuccess = (dispatch, notifications) => {
  dispatch({
    type: FETCH_NOTIFICATIONS_SUCCESS,
    payload: notifications,
  });
};

export const fetchNotifications = (uid) => async (dispatch) => {
  try {
    dispatch({ type: FETCH_NOTIFICATIONS });
    usersCollection.doc(uid).collection('notifications').orderBy('timestamp', 'desc').onSnapshot((snap) => {
      fetchNotificationsSuccess(dispatch, snap.docs.map((doc) => doc.data()));
    });
  } catch (error) {
    fetchNotificationsFail(dispatch, error);
  }
};
