import { SIGNUP_USER, SIGNUP_USER_SUCCESS, SIGNUP_USER_FAIL } from '../constants';
import firebase from '../config/firebase';

const signUpUserSuccess = (dispatch, user) => {
  dispatch({
    type: SIGNUP_USER_SUCCESS,
    payload: user,
  });
};

const signUpUserFail = (dispatch, error) => {
  dispatch({
    type: SIGNUP_USER_FAIL,
    payload: error,
  });
};

export const signUpUser = (email, password) => async (dispatch) => {
  dispatch({ type: SIGNUP_USER });
  try {
    const { user } = await firebase.auth().signInWithEmailAndPassword(email, password);
    return signUpUserSuccess(dispatch, user);
  } catch (error) {
    dispatch({ type: SIGNUP_USER });
    return signUpUserFail(dispatch, error);
  }
};
