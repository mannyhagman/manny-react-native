import { SIGN_OUT_USER } from '../constants';
import firebase from '../config/firebase';

export const signOutUser = () => async (dispatch) => {
  dispatch({ type: SIGN_OUT_USER });
  try {
    firebase.auth().signOut();
  } catch (error) {
    console.error(error);
  }
};
