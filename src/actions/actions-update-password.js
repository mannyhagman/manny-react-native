import { UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAIL } from '../constants';
import firebase from '../config/firebase';

const updatePasswordSuccess = (dispatch, message) => {
  dispatch({
    type: UPDATE_PASSWORD_SUCCESS,
    payload: message,
  });
};

const updatePasswordFail = (dispatch, error) => {
  dispatch({
    type: UPDATE_PASSWORD_FAIL,
    payload: error,
  });
};

export const updatePassword = (password) => async (dispatch) => {
  try {
    dispatch({ type: UPDATE_PASSWORD });
    const result = await firebase.auth().currentUser.updatePassword(password);
    updatePasswordSuccess(dispatch, result);
  } catch (error) {
    return updatePasswordFail(dispatch, error);
  }
};
