import { emailChanged, passwordChanged, confirmPasswordChanged } from './actions-inputs';
import { loginUser, clearMessage } from './actions-login';
import { fetchNotifications } from './actions-notifications';
import { resetPassword } from './actions-reset-password';
import { signUpUser } from './actions-signup';
import { updatePassword } from './actions-update-password';
import { signOutUser } from './actions/signout';

export default {
  emailChanged,
  passwordChanged,
  confirmPasswordChanged,
  loginUser,
  clearMessage,
  fetchNotifications,
  resetPassword,
  signUpUser,
  signOutUser,
  updatePassword,
};
