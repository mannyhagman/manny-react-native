import { RESET_PASSWORD, RESET_PASSWORD_SUCCESS, RESET_PASSWORD_FAIL } from '../constants';
import firebase from '../config/firebase';

const resetPasswordSuccess = (dispatch, message) => {
  dispatch({
    type: RESET_PASSWORD_SUCCESS,
    message,
  });
};

const resetPasswordFail = (dispatch, error) => {
  dispatch({
    type: RESET_PASSWORD_FAIL,
    error,
  });
};

export const resetPassword = (email) => {
  return (dispatch) => {
    dispatch({ type: RESET_PASSWORD });
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then((message) => resetPasswordSuccess(dispatch, message))
      .catch((error) => resetPasswordFail(dispatch, error));
  };
};
