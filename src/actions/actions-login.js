import { LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, CLEAR_MESSAGE, VERIFY_AUTH_STATE } from '../constants';
import firebase from '../config/firebase';

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user,
  });
};

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error,
  });
};

export const signOutUser = () => {
  return (dispatch) => {
    dispatch({ type: SIGNOUT_USER });
    firebase.auth().signOut();
  };
};

export const verifyAuthState = () => {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        loginUserSuccess(dispatch, user);
      } else {
        signOutUser();
      }
    });
  };
};

export const loginUser = (email, password) => async (dispatch) => {
  dispatch({ type: LOGIN_USER });
  try {
    const { user } = await firebase.auth().signInWithEmailAndPassword(email, password);
    return loginUserSuccess(dispatch, user);
  } catch (error) {
    return loginUserFail(dispatch, error);
  }
};

export const clearMessage = (message) => {
  return {
    type: CLEAR_MESSAGE,
    message,
  };
};
